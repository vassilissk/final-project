"""there are all views in project"""
from datetime import datetime
from flask import render_template, url_for, request, redirect
from setup import app, db
from models.dbmodels import Department, Employee


def validation():
    try:
        date_of_birth = datetime.strptime(request.form.get('birth'), '%Y-%m-%d')

    except ValueError:
        error = 'Enter correct date in format YY-MM-DD, please'
        return render_template('add_employee.html', error=error)

    try:
        salary = int(request.form.get('salary', False))
    except ValueError:
        error = 'Salary must be an integer'
        return render_template('add_employee.html', error=error)


@app.route('/')
@app.route('/homepage')
def homepage():
    """function shows the main page"""
    print(url_for('homepage'))
    return render_template('homepage.html')


@app.route('/departments')
def departments():
    """function shows the departments
        and calculate the average salary"""
    res = Department.query.all()
    salary_list = []
    for dep in res:
        sum_salary = 0
        counter = 0
        list_of_employees = Employee.query.filter(Employee.department_id == dep.id)
        for i in list_of_employees:
            sum_salary += i.salary
            counter += 1
        if counter != 0:
            salary_list.append(round(sum_salary / counter, 2))
        else:
            salary_list.append(0)
    result = zip(res, salary_list)
    return render_template('departments.html', result=result)


@app.route('/add_dep', methods=['POST', 'GET'])
def add_dep():
    """ Adding  department to the list of departments """
    print(request.method)
    if request.method == 'POST':
        dep = Department(name=request.form.get('name', False))
        db.session.add(dep)
        db.session.commit()
        return redirect('departments')
    return render_template('add_dep.html')


@app.route('/edit_dep/<dep_id>', methods=['POST', 'GET'])
def edit_dep(dep_id):
    """You can change the name of department"""
    edited_dep = Department.query.filter(Department.id == dep_id)[0]
    if request.method == 'POST':
        dep = Department(name=request.form.get('name', False))
        edited_dep.name = dep.name
        db.session.commit()

        return redirect(url_for('departments'))
    return render_template('edit_dep.html', dep_name=edited_dep.name)


@app.route('/departments/<dep_id>')
def department(dep_id):
    """Shows the list of employees in concrete department"""
    dep_name = Department.query.filter(Department.id == dep_id)[0].name
    res = Employee.query.filter(Employee.department_id == dep_id)
    return render_template('department.html', dep_id=dep_id, res=res, dep_name=dep_name)


@app.route('/profile/<user_id>')
def profile(user_id):
    """Shows teh profile of concrete employee"""
    res = Employee.query.filter(Employee.id == user_id)
    return render_template('profile.html', res=res[0], name_of_department=Department.query \
                           .filter(Department.id == res[0].department_id)[0].name)


@app.route('/del_dep/<dep_id>', methods=['GET'])
def del_dep(dep_id):
    """First deleting all employees from department,
    then deleting the department"""
    res = Employee.query.filter(Employee.department_id == dep_id)
    for i in res:
        db.session.delete(i)
    db.session.commit()
    res = Department.query.filter(Department.id == dep_id)
    db.session.delete(res[0])
    db.session.commit()

    return redirect(url_for('departments'))


@app.route('/department/<dep_id>/add_employee', methods=['POST', 'GET'])
def add_employee(dep_id):
    """You can add new employee to the department"""
    if request.method == 'POST':

        try:
            date_of_birth = datetime.strptime(request.form.get('birth'), '%Y-%m-%d')

        except ValueError:
            error = 'Enter correct date in format YY-MM-DD, please'
            return render_template('add_employee.html', error=error)

        try:
            salary = int(request.form.get('salary', False))
        except ValueError:
            error = 'Salary must be an integer'
            return render_template('add_employee.html', error=error)
        # name = request.form.get('name', False)
        employee = Employee(department_id=dep_id,
                            name=request.form.get('name', False),
                            date_of_birth=datetime.strptime(request.form.get('birth'), '%Y-%m-%d'),
                            salary=request.form.get('salary', False)
                            )
        db.session.add(employee)
        db.session.commit()
        return redirect(url_for('department', dep_id=dep_id))
    return render_template('add_employee.html')


@app.route('/del_profile/<profile_id>', methods=['GET'])
def del_profile(profile_id):
    """Deleting profile from database"""
    res = Employee.query.filter(Employee.id == profile_id)
    temp = res[0].department_id
    db.session.delete(res[0])
    db.session.commit()
    return redirect(url_for('department', dep_id=temp))


@app.route('/edit_profile/<profile_id>', methods=['POST', 'GET'])
def edit_profile(profile_id):
    """You can edit profile """
    edited_profile = Employee.query.filter(Employee.id == profile_id)[0]
    if request.method == 'POST':
        try:
            date_of_birth = datetime.strptime(request.form.get('birth'), '%Y-%m-%d')

        except ValueError:
            error = 'Enter correct date in format YY-MM-DD, please'
            return render_template('edit_profile.html', error=error)

        try:
            salary = int(request.form.get('salary', False))
        except ValueError:
            error = 'Salary must be an integer'
            return render_template('edit_profile.html', error=error)
        employee = Employee(department_id=request.form.get('department', False),
                            name=request.form.get('name', False),
                            date_of_birth=datetime.strptime(request.form.get('birth'), '%Y-%m-%d'),
                            salary=request.form.get('salary', False))

        edited_profile.name = employee.name
        edited_profile.department_id = employee.department_id
        edited_profile.date_of_birth = employee.date_of_birth
        edited_profile.salary = employee.salary

        db.session.commit()

        return redirect(url_for('profile', user_id=profile_id))
    return render_template('edit_profile.html', name=edited_profile.name,
                           department_id=edited_profile.department_id,
                           date_of_birth=edited_profile.date_of_birth,
                           salary=edited_profile.salary)


@app.route('/search/<dep_id>', methods=['POST'])
def search(dep_id):
    """Searches for employees born on a certain date or
    in the period between first and second dates """
    first = request.form.get('first', False)
    second = request.form.get('second', False)
    if second:
        res = Employee.query.filter(Employee.department_id == dep_id). \
            filter(Employee.date_of_birth >= first). \
            filter(Employee.date_of_birth <= second)
    else:
        res = Employee.query.filter(Employee.department_id == dep_id). \
            filter(Employee.date_of_birth == first)

    return render_template('search.html', res=res)
